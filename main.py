from datetime import datetime
from mysql.connector import (connection)
import requests
from config import db_host, db_user, db_password, db_database, api_url, authorization_token

db_connection = connection.MySQLConnection(
    host=db_host, user=db_user, password=db_password, database=db_database)

cursor = db_connection.cursor()

response = requests.get(api_url)
orders = response.json()['orders']
updated_at = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
for order in orders:
    cursor.execute(
        "SELECT * FROM orders WHERE `order` = '"+order['name'].strip()+"' LIMIT 1")
    found = cursor.fetchone()
    # print(order['name'])
    # print(cursor.statement)
    # print(found)
    # break

    values = (
        order['name'].strip(),
        order['customer']['first_name'] + ' ' + order['customer']['last_name'],
        order['customer']['email'],
        order['customer']['phone'],
        order['total_price'],
        order['financial_status'],
        '',
        len(order['line_items']),
        order['shipping_lines'][0]['title'] if len(
            order['shipping_lines']) > 0 else 'pendente',
        order['tags'],
        order['created_at'],
        updated_at,
    )
    percentages = ("%s, " * len(values)).strip(', ')

    if found:
        sql = "UPDATE orders SET `order` = %s, customer_name = %s, customer_email = %s, customer_phone = %s, total = %s, payment = %s, fulfillment = %s, items = %s, delivery_method = %s, tags = %s, created_at = %s, updated_at = %s WHERE id = '" + \
            str(found[0]) + "'"
    else:
        sql = 'INSERT INTO orders(`order`, customer_name, customer_email, customer_phone, total, payment, fulfillment, items, delivery_method, tags, created_at, updated_at) VALUES(' + percentages + ')'

    cursor.execute(sql, values)
    db_connection.commit()

db_connection.close()
